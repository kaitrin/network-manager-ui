import { NetworkManagerUiPage } from './app.po';

describe('network-manager-ui App', () => {
  let page: NetworkManagerUiPage;

  beforeEach(() => {
    page = new NetworkManagerUiPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
